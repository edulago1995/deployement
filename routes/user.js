//Route

//Dependencies and Modules

const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

//Routing Component
const router = express.Router();

//Routes - POST
//Check email
router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController))
})

//Register a user
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})
//router,post("/register",userController.registerUser);

//User Authentication

router.post("/login",userController.loginUser);

//[ACTIVITY] Route for retrieving user details
router.post("/details", verify, userController.getProfile);

// Route to enroll user to a course
router.post("/enroll", verify, userController.enroll);

//Export Route System
module.exports = router;