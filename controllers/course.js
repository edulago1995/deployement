const Course = require("../models/Course");

//Create a new course
/*
    Steps:
    1. Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
    2. Uses the information from the request body to provide all the necessary information.
    3. Saves the created object to our database and add a successful validation true/false.
*/
module.exports.addCourse = (req, res) => {

    // Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
    // Uses the information from the request body to provide all the necessary information
    let newCourse = new Course({
        name : req.body.name,
        description : req.body.description,
        price : req.body.price
    });

    // Saves the created object to our database
    return newCourse.save().then((course, error) => {

 
        if (error) {
            return res.send(false);

 
        } else {
            return res.send(true);
        }
    })
    .catch(err => res.send(err))
};

//Retrieve all courses
/*
	1. Retrieve all the courses from the database
*/

//We will use the find() method for our course model

module.exports.getAllCourses = (req,res)=>{
	return Course.find({}).then(result=>{
		return res.send(result)
	})
}

//getAllActiveCourses
//create a function that will handle req,res
//that will find all active courses in the db

module.exports.getAllActiveCourses = (req,res)=>{
	return Course.find({isActive:true}).then(result=>{
		return res.send(result)
	})
}
//Get a specific course
module.exports.getCourse = (req,res)=>{
	return Course.findById(req.params.courseId).then(result=>{
		return res.send(result)
	})
}

module.exports.updateCourse = (req,res)=>{

	let updatedCourse = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course, error)=>{
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
}

module.exports.archiveCourse = (req, res) => {

    let updateActiveField = {
        isActive: false
    }

    return Course.findByIdAndUpdate(req.params.courseId, updateActiveField)
    .then((course, error) => {

        //course archived successfully
        if(error){
            return res.send(false)

        // failed
        } else {
            return res.send(true)
        }
    })
    .catch(err => res.send(err))

};

module.exports.activateCourse = (req, res) => {

    let updateActiveField = {
        isActive: true
    }

    return Course.findByIdAndUpdate(req.params.courseId, updateActiveField)
    .then((course, error) => {

        //course archived successfully
        if(error){
            return res.send(false)

        // failed
        } else {
            return res.send(true)
        }
    })
    .catch(err => res.send(err))

};